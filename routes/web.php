<?php

use App\Mail\InviteUser;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@index');
Route::get('/about', 'PagesController@about');
Route::get('/contact', 'PagesController@contact');


/* 

GET     /projects (index)
GET     /projects/create (create)
GET     /projects/1 (show)
POST    /projects (store)
GET     /projects/1/edit (edit)
PATCH   /projects/1 (update)
DELETE  /projects/1 (destroy)

*/





Route::get('/pages/{page}', 'CreatePagesController@show');
Route::post('/pages', 'CreatePagesController@store');
Route::get('/pages/{page}/edit', 'CreatePagesController@edit');
Route::patch('/pages/{page}', 'CreatePagesController@update');
Route::delete('/pages/{page}', 'CreatePagesController@destroy');

Route::group(['middleware' => ['auth']], function () {


    Route::get('/projects', 'ProjectsController@index');
    Route::get('/projects/create', 'ProjectsController@create');
    Route::post('/projects', 'ProjectsController@store');
    Route::get('/projects/{project}/edit', 'ProjectsController@edit');
    Route::patch('/projects/{project}', 'ProjectsController@update');
    Route::delete('/projects/{project}', 'ProjectsController@destroy');
    Route::get('/projects/{project}', 'ProjectsController@show');

    Route::post('/projects/{project}/tasks', 'ProjectTasksController@store');

    Route::get('/projects/{project}/tasks', 'ProjectTasksController@index');
    Route::get('/tasks/{task}', 'ProjectTasksController@show');
    Route::put('/tasks/{task}', 'ProjectTasksController@update');
    Route::delete('/tasks/{task}', 'ProjectTasksController@destroy');
    Route::get('/dashboard', 'HomeController@index')->name('dasboard');

    Route::group(['prefix' => 'admin'], function () {
        Route::get('/panel', 'AdminController@index')->name('admin');
        Route::get('/pages', 'CreatePagesController@index');
        Route::get('/pages/create', 'CreatePagesController@create');
    });
});








Route::post('/projects/collaboration/invite', 'ProjectsController@invite');


Auth::routes();
