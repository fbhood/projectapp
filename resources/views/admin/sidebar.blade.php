<div class="p-0 ">
    <div style="text-align:right" class="bg-dark">

        <button class="btn btn-dark" type="button" data-toggle="collapse" data-target="#admin_panel_sidebar" aria-expanded="false"
                aria-controls="admin_panel_sidebar">
                
                <i class="fas fa-chevron-circle-left"></i><br>
                <small>Sidebar</small>
                
        </button>
    </div>
    <div class="collapse show col-12 bg-dark" id="admin_panel_sidebar">

        <div style="height:100vh" >
    
    
            <aside id="admin_panel" style="padding-top:1em"> 
                <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                    <a class="nav-link active" id="v-pills-admin-tab" data-toggle="pill" href="#v-pills-admin" role="tab" aria-controls="v-pills-dashboad" aria-selected="true"><i class="fas fa-brain"></i> Admin Panel</a>
                    
                    <hr class="bg-light">
                    
                    <a class="nav-link" id="v-pills-pages-tab" data-toggle="pill" href="#v-pills-pages" role="tab" aria-controls="v-pills-pages" aria-selected="false"><i class="fas fa-scroll"></i> Pages</a>
                    <a class="nav-link" id="v-pills-projects-tab" data-toggle="pill" href="#v-pills-projects" role="tab" aria-controls="v-pills-projects" aria-selected="false"><i class="fas fa-project-diagram"></i> Projects</a>
                    <a class="nav-link" id="v-pills-users-tab" data-toggle="pill" href="#v-pills-users" role="tab" aria-controls="v-pills-users" aria-selected="false"><i class="fas fa-users"></i> Users</a>
                    <a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-selected="false"><i class="fas fa-cogs"></i> General Settings</a>
                </div>
                
                <hr>
                


            </aside>
        </div>

    </div>

</div>