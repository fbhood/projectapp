@extends('layouts.app')

@section('content')


<div class="container-fluid">
    <div class="row">
       

       @include('admin.sidebar')

        <div class="col py-4">
            <div class="tab-content" id="v-pills-tabContent">
                <div class="tab-pane fade show active" id="v-pills-admin" role="tabpanel" aria-labelledby="v-pills-admin-tab">
                    <div class="card">
                        <div class="card-header">
                        <span class="fa-stack" style="vertical-align: top;">
                            <i class="far fa-circle fa-stack-2x"></i>
                            <i class="fas fa-brain fa-stack-1x"></i>
                        </span>
                        Administrator Panel</div>

                        <div class="card-body">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif

                            You are logged in! Create a <a href="/admin/pages/create">New Page </a>

                            <hr>
                           
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="v-pills-pages" role="tabpanel" aria-labelledby="v-pills-pages-tab">
                    <h1>Pages</h1>
                    <div id="adminPagesAccordion" role="tablist" aria-multiselectable="true">
                        <div class="p-2">
                            <div class="card-header" role="tab" id="CreatePage">
                                <h5 class="mb-0">
                                    <a data-toggle="collapse" data-parent="#adminPagesAccordion" href="#createNewPage" aria-expanded="true" aria-controls="createNewPage">
                              Create a new page
                            </a>
                                </h5>
                            </div>
                            <div id="createNewPage" class="collapse in" role="tabpanel" aria-labelledby="CreatePage">
                                <div class="card-body">
                                    New Page
                                    <div class="container">

                                        <form action="/admin/pages" method="post">
                                            @csrf
                                            <div class="form-group">
                                            <label for="title">Title</label>
                                            <input type="text" class="form-control" name="title" id="title" aria-describedby="HelpPageTitle" placeholder="Page Title">
                                            <small id="HelpPageTitle" class="form-text text-muted">Add your page title</small>
                                            </div>

                                            <div class="form-group">
                                            <label for="description">Description</label>
                                            <textarea class="form-control" name="description" id="description" rows="5"></textarea>
                                            </div>
                                            <div class="form-group">
                                            <label for="image">Upload Image</label>
                                            <input type="file" class="form-control-file" name="image" id="image" placeholder="Image upload" aria-describedby="imageId">
                                            <small id="imageId" class="form-text text-muted">Help text</small>
                                            </div>
                                            <div class="form-group">
                                            <label for="status">Select Status</label>
                                            <select class="form-control" name="status" id="status">
                                                <option>Publish</option>
                                                <option>Draft</option>
                                                <option>Private</option>
                                            </select>
                                            </div>
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-info">Save</button>

                                            </div>

                                        </form>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                        <table class="table">
                            <thead class="">
                                <tr>
                                    <th scope="col">#ID</th>
                                    <th>Title</th>
                                    <th>Dscription</th>
                                    <th>Author ID</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                            
                                @foreach($pages as $page )
                                    <tr>
                                        <td scope="row">
                                            {{$page->id}}
                                        </td>
                                        <td >
                                            <a href="/pages/{{ $page->id }}">{{$page->title}} </a>
                                        </td>
                                        <td>
                                            {{$page->description}}
                                        </td>
                                        <td>
                                            {{$page->user_id}}
                                        </td>
                                        <td>{{$page->status}}</td>
                                        <td>
                                        
                                            <a href="/pages/{{ $page->id }}" class="btn btn-dark float-left mr-1"> <i class="far fa-eye"></i></a>

                                            @auth

                                                <a href="/pages/{{ $page->id }}/edit" class="btn btn-primary float-left mr-1"><i class="fas fa-edit"></i></a>
                                                
                                                <form action="/pages/{{ $page->id }}" method="post">
                                                    @csrf
                                                    @method('DELETE')
                                                        
                                                        <button type="submit" class="btn btn-danger float-left"><i class="far fa-trash-alt"></i> </button>  
                                                </form>
                                            @endauth

                                        </td>


                                    </tr>

                                @endforeach
                        
                            </tbody>
                        </table>
                           
                </div>
            

                <div class="tab-pane fade" id="v-pills-projects" role="tabpanel" aria-labelledby="v-pills-projects-tab">
                    <h1>Projects</h1>
                    <div id="adminProjectsAccordion" role="tablist" aria-multiselectable="true">
                        <div class="p-2">
                            <div class="card-header" role="tab" id="CreateProject">
                                <h5 class="mb-0">
                                    <a  data-toggle="collapse" data-parent="#adminProjectsAccordion" href="#createNewProject" aria-expanded="true" aria-controls="createNewProject">
                              Create a new project
                            </a>
                                </h5>
                            </div>
                            <div id="createNewProject" class="collapse in " role="tabpanel" aria-labelledby="CreateProject">
                                <div class="card-body">
                                    New Project
                                    <div class="container">

                                        <form action="/projects" method="POST">
                                            @csrf
                                            <div class="form-group">
                                                <label for="title">Add a Title</label>
                                                <input type="text" name="title" id="project-itle" class="form-control {{ $errors->has('title') ? 'alert-danger' : ''}}" value="{{ old('title') }}" placeholder="My awesome project" aria-describedby="titleHelper" >
                                                <small id="titleHelper" class="text-muted">Insert a title for the current project</small>
                                            </div>

                                            <div class="form-group">
                                                <label for="description">Project Description</label>
                                                <textarea class="form-control {{ $errors->has('description') ? 'alert-danger' : ''}}" value="{{ old('description') }}"  name="description" id="description" rows="4" aria-describedby="descriptionHelper" ></textarea>
                                                <small id="descriptionHelper" class="text-muted">Insert a title for the current project</small>

                                            </div>
                                            <div class="container">
                                                <div class="row">
                                                    <div class="col-6">
                                                        <div class="form-group">
                                                        
                                                        <button type="submit" class="btn btn-primary">Save Project</button>

                                                        </div>
                                                    </div>
                                                    <div class="col-6">
                                                

                                                    </div>
                                                    
                                                </div>
                                            </div>
                                            

                                            
                                        </form>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                        <table class="table">
                            <thead class="">
                                <tr>
                                    <th scope="col">#ID</th>
                                    <th>Author_id</th>
                                    <th>Title</th>
                                    <th>Description</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                            
                                @foreach($projects as $project )
                                    <tr>
                                        <td scope="row">
                                            {{$project->id}}
                                        </td>
                                        <td>
                                            {{$project->user_id}}
                                        </td>
                                        <td >
                                            <a href="/projects/{{ $project->id }}">{{$project->title}} </a>
                                        </td>
                                        <td>
                                            {{$project->description}}
                                        </td>
                                        
                                        <td>
                                        
                                            <a href="/projects/{{ $project->id }}" class="btn btn-dark float-left mr-1"> <i class="far fa-eye"></i></a>

                                            @auth

                                                <a href="/projects/{{ $project->id }}/edit" class="btn btn-primary float-left mr-1"><i class="fas fa-edit"></i></a>
                                                
                                                <form action="/projects/{{ $project->id }}" method="post">
                                                    @csrf
                                                    @method('DELETE')
                                                        
                                                        <button type="submit" class="btn btn-danger float-left"><i class="far fa-trash-alt"></i></button>  
                                                </form>
                                            @endauth

                                        </td>


                                    </tr>

                                @endforeach
                        
                            </tbody>
                        </table>
                           
                </div>


                <div class="tab-pane fade" id="v-pills-users" role="tabpanel" aria-labelledby="v-pills-users-tab">
                    <h1>Users</h1>

                    <table class="table">
                        <thead>
                            <tr>
                                <th>User ID</th>
                                <th>User Name</th>
                                <th>User Email</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($users as $user) 

            
                            <tr>
                                <td scope="row">{{$user->id}}</td>
                                <td>{{ $user->name }}</td>
                                <td> {{ $user->email }} </td>
                            </tr>
                          
                      

                            @endforeach
                        </tbody>
                    </table>

                </div>
                <div class="tab-pane fade" id="v-pills-settings" role="tabpanel" aria-labelledby="v-pills-settings-tab">
                    <h1>Settings</h1>
                </div>
            </div>
        </div>
    </div>
  
</div>
@endsection
