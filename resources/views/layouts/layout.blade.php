@include('partials.head')
  
        <div id="app">
            @include('partials.nav')

            
                
            <main class="">
                @yield('content')
            </main>
            
            @include('partials.footer')

    
        </div>

@include('partials.bodyend')
