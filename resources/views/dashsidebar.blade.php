<div class="">
    <div style="text-align:right" class="bg-white">

        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#dashboard_sidebar" aria-expanded="false"
                aria-controls="dashboard_sidebar">
                
                <i class="fas fa-chevron-circle-left"></i><br>
                <small>Sidebar</small>
                
        </button>
    </div>
    <div class="collapse show col-12 bg-white" id="dashboard_sidebar">

        <div style="height:100vh" >
    
    
            <aside id="dashboard" style="padding-top:1em"> 
                <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                    <a class="nav-link active" id="v-pills-dashboard-tab" data-toggle="pill" href="#v-pills-dashboard" role="tab" aria-controls="v-pills-dashboad" aria-selected="true">
                        <i class="fas fa-tachometer-alt"></i>  Dashboard</a>
                    <hr class="bg-light text-white">
                    <a class="nav-link" id="v-pills-projects-tab" data-toggle="pill" href="#v-pills-projects" role="tab" aria-controls="v-pills-projects" aria-selected="false"><i class="fas fa-project-diagram"></i> Projects</a>
                    <a class="nav-link" id="v-pills-tasks-tab" data-toggle="pill" href="#v-pills-tasks" role="tab" aria-controls="v-pills-tasks" aria-selected="false"><i class="fas fa-tasks"></i> Tasks</a>
                    <a class="nav-link" id="v-pills-collaborators-tab" data-toggle="pill" href="#v-pills-collaborators" role="tab" aria-controls="v-pills-collaborators" aria-selected="false"><i class="fas fa-user-shield"></i> Collaborators</a>
                    <a class="nav-link" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-selected="false"><i class="fas fa-comments"></i> Messages</a>
                    <a class="nav-link" id="v-pills-reports-tab" data-toggle="pill" href="#v-pills-reports" role="tab" aria-controls="v-pills-reports" aria-selected="false"><i class="fas fa-chart-pie"></i> Reports</a>
                    <a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-selected="false"><i class="fas fa-sliders-h"></i> Settings</a>
                </div>
                
                <hr>
                
                <example-component></example-component>


            </aside>
        </div>

    </div>

</div>