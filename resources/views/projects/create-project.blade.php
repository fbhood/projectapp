@extends('layouts.layout')

@section('title')

   New Project | ProjectsApp

@endsection

@section('content')

<div class="jumbotron jumbotron-fluid">
    <div class="container">
        <h1 class="display-3">Create a Project</h1>
        
    </div>
</div>

    @if($errors->any())
    <div class="alert alert-danger" role="alert">
        <strong>
        
        <ul>
        @foreach($errors->all() as $error) 

            <li>{{$error}}</li>

        @endforeach

        </ul>
        
        </strong>
    </div>
   @endif
   <div class="container-fluid">
       
        <form action="/projects" method="POST">
            @csrf
            <div class="form-group">
                <label for="title">Add a Title</label>
                <input type="text" name="title" id="project-itle" class="form-control {{ $errors->has('title') ? 'alert-danger' : ''}}" value="{{ old('title') }}" placeholder="My awesome project" aria-describedby="titleHelper" >
                <small id="titleHelper" class="text-muted">Insert a title for the current project</small>
            </div>

            <div class="form-group">
                <label for="description">Project Description</label>
                <textarea class="form-control {{ $errors->has('description') ? 'alert-danger' : ''}}" value="{{ old('description') }}"  name="description" id="description" rows="4" aria-describedby="descriptionHelper" ></textarea>
                <small id="descriptionHelper" class="text-muted">Insert a title for the current project</small>

            </div>
            <div class="container">
                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                        
                        <button type="submit" class="btn btn-primary">Save Project</button>

                        </div>
                    </div>
                    <div class="col-6">
                

                    </div>
                    
                </div>
            </div>
            

            
        </form>
    </div>

  
@endsection