@extends('layouts.layout')

@section('title')

    Projects | ProjectsApp

@endsection

@section('content')

<div class="jumbotron jumbotron-fluid">
    <div class="container">
        <h5>Editing Project</h5>
        <h1 class="display-3"> {{ $project->title}}</h1>
        <a class="btn btn-primary" href="/projects/{{$project->id}}">View Project</a>        
        <button form="edit_form" type="submit" class="btn btn-dark">Update</button>  
        <button form="delete_form" type="submit" class="btn btn-danger">Delete</button>  


    </div>
</div>    
    </div>
     @if($errors->any())
    <div class="alert alert-danger" role="alert">
        <strong>
        
        <ul>
        @foreach($errors->all() as $error) 

            <li>{{$error}}</li>

        @endforeach

        </ul>
        
        </strong>
    </div>
   @endif
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-8">
                
            @if($project->user_id === auth()->id())
    
    
                <form id="edit_form" action="/projects/{{ $project->id }}" method="post">
                    @csrf
                    @method('PATCH')
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" name="title" id="project-title" class="form-control" value="{{$project->title}}" aria-describedby="helpTitle" required>
                        <small id="helpTitle" class="text-muted">Update the title here</small>
                    </div>
                    
                    <div class="form-group">
                        <label for="description">Description</label>
                        <textarea class="form-control" name="description" id="description" rows="6" aria-describedby="helpDescription" required>{{$project->description}}</textarea>
                        <small id="helpDescription" class="text-muted">Update the description here</small>

                    </div>

                </form>


                <form id="delete-form" action="/projects/{{ $project->id }}" method="post">
                    @csrf
                    @method('DELETE')
                        
                </form>

            @endif
            </div>

             <div id="sidebar-project-edit" class="col-xs-12 col-4">
            
                <h5>Add tasks</h5>           

                <form action="/projects/{{ $project->id }}/tasks" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="body">Add a body</label>
                        <input type="text" name="body" id="task-body" class="form-control {{ $errors->has('body') ? 'alert-danger' : ''}}" value="{{ old('body') }}" placeholder="A task for this project" aria-describedby="bodyHelper" required>
                        <small id="bodyHelper" class="text-muted">Insert a title for the current project</small>
                    </div>

                
                    <button type="submit" class="btn btn-primary">Add Task</button>    
                        
                </form>
                @if($project->tasks())
                    
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Task</th>
                                <th>Status</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>

        

                            @foreach($project->tasks as $task)

                                <tr>
                                    <td scope="row">{{$task->body}}</td>
                                    <td>
                                        @if(! $task->completed  === false)  
                                            <span class="" role="alert">
                                                Complete
                                            </span>
                                        @else 
                                            <span class="" role="alert">
                                            Incomplete
                                            </span>
                                        @endif
                                    </td>


                                    
                                    <td>
                                        <form action="/tasks/{{ $task->id}}" method="post">
                                            @csrf 
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger"><i class="far fa-trash-alt"></i></button>
                                        </form>
                                    </td>
                                
                                
                                
                                </tr>

                            @endforeach
                        </tbody>
                    </table>

                @endif


            </div><!--End Sidebar-->
            

        </div>
    </div>




@endsection