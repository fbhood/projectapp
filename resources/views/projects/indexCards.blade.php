@extends('layouts.layout')

@section('title')

Projects | ProjectsApp

@endsection

@section('content')

<div class="jumbotron jumbotron-fluid">
    <div class="container">
        <h1 class="display-3">Projets </h1>
        <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Harum, iure!</p>

    </div>
</div>

<div class="container">


    <div class="row">
        @forelse($projects as $project)


        <div class="col-3 p-2">
            <div class="card p-0">
                <img class="card-img-top" src="svg/card-img.svg" alt="Card image cap">
                <div class="card-body p-4">
                    <p class="card-text"><small class="text-muted">ID#{{$project->id}} </small></p>
                    <h5 class="card-title"><a href="{{$project->path()}}">{{str_limit($project->title,20)}} </a></h5>
                    <p class="card-text">{{ str_limit($project->description, 40)}}</p>
                </div>
                <hr>
                <footer class="p-2 mb-2">
                    <a href="{{$project->path()}}" class="btn btn-dark float-left mr-1"> <i class="far fa-eye"></i> View</a>

                    @auth
                    @if($user->can(['edit','delete'], $project))

                    <a href="/projects/{{$project->id}}/edit" class="btn btn-primary float-left mr-1"><i class="fas fa-edit"></i> Edit</a>

                    <form action="{{$project->path()}}" method="post">
                        @csrf
                        @method('DELETE')

                        <button type="submit" class="btn btn-danger float-left"><i class="far fa-trash-alt"></i> Delete</button>
                    </form>
                    @endif
                    @endauth

                </footer>
            </div>
        </div>

        @empty
        <p> No projects yet</p>
        @endforelse


    </div>




</div>
<hr>
@endsection