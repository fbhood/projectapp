@extends('layouts.layout')

@section('title')

    Projects | ProjectsApp

@endsection

@section('content')

  <div class="jumbotron">
    <h1 class="display-3"> {{ $project->title }} 
      <sup style="font-size:large;" class="badge badge-pill badge-info"> 
        {{ count($project->tasks->all())}} <br>
      <small>Tasks</small>
      </sup>  
    </h1> 
  
    <p> {{ $project->description}} </p>
   @auth
    @if($project->user_id === auth()->id())

      <a href="/projects/{{$project->id}}/edit" class="link link-primary">Edit Project</a> 
    @endif
    
    @else
      <a href="/login">Login to Edit</a>
    @endauth
  </div>
<div class="container">
  <div class="row">

    @auth
      @if($project->user_id === auth()->id())

        <div class="col-8">
          @if($project->tasks())
            <table class="table">
              <thead>
                <tr>
                  <th>Tasks</th>
                  <th>Date</th>
                  <th>Status</th>
                  <th>View</th>

                </tr>
              </thead>
              <tbody>
              @foreach($project->tasks as $task)
                <tr>
                  <td scope="row">  

                      <form action="/tasks/{{ $task->id }}" method="post">
                        @csrf
                        @method('PUT')
                        <div class="form-check">

                          <label class="form-check-label">
                          
                            <input style="height:30px; width: 30px;" type="checkbox" class="form-check-input" name="completed" id="checkbox-task-{{$task->id}}" value="" onChange="this.form.submit()" {{$task->completed ? 'checked' : ''}}>
                            <span style="margin-left:2em">{{$task->body}}</span>

                  

                          </label>
                        </div>
                
                      </form>
                
                    </td>
                  <td>
                  <small class=""> <i class="far fa-calendar-alt"></i>  {{$task->updated_at}}</small>          
                  </td>
                    <td>
                      <div class="status ">
                          @if(! $task->completed  === false)  
                            <div class="btn btn-success" role="alert">
                                <strong>Complete</strong>
                            </div>
                          @else 
                            <div class="btn btn-danger" role="alert">
                              <strong>Incomplete</strong>
                            </div>
                          @endif
                      </div>
                    </td>

                    <td>
                      <a href="/tasks/{{$task->id}}"><i class="fas fa-eye fa-2x fa-fw"></i></a>

                    </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          @endif
      
        </div>
        
        <div class="col-4">
          <h5>Add tasks</h5>           

          <form action="/projects/{{ $project->id }}/tasks" method="POST">
              @csrf
              <div class="form-group">
                  <label for="body">Add a body</label>
                  <input type="text" name="body" id="task-body" class="form-control {{ $errors->has('body') ? 'alert-danger' : ''}}" value="{{ old('body') }}" placeholder="A task for this project" aria-describedby="bodyHelper" required>
                  <small id="bodyHelper" class="text-muted">Insert a title for the current project</small>
              </div>

          
              <button type="submit" class="btn btn-primary">Add Task</button>    
                  
          </form>
              
        </div>

      @endif
    @else

    <div class="col-12">
      
      @if($project->tasks())
        <ul class="list-group container">
          @foreach($project->tasks as $task)
            <li class="list-group-item row">
              
                <strong style="margin-left:2em">  {{$task->body}} </strong>


              <div class="col-4 float-right">
              @if(! $task->completed  === false)  
                <div class="btn btn-success" role="alert">
                    <strong>Complete</strong>
                </div>
              @else 
                <div class="btn btn-danger" role="alert">
                  <strong>Incomplete</strong>
                </div>
              @endif
              
              </div>
            </li>
          @endforeach
        </ul>
      @endif
    </div>
    @endauth


  
  </div>
</div>



@endsection