@extends('layouts.layout')

@section('title')

Projects | ProjectsApp

@endsection

@section('content')

<div class="jumbotron jumbotron-fluid">
    <div class="container">
        <h1 class="display-3">Projets </h1>
        <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Harum, iure!</p>

    </div>
</div>

<div class="container">


    <ul class="list-group">

        @forelse($projects as $project)
        <li class="list-group-item d-flex justify-content-between align-items-center is-active">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-1">
                        <span class="badge badge-secondary badge-pill">ID #{{$project->id}} <!-- TODO: Return the number of tasks instead of the project id--><br>


                            <br>
                        </span>
                    </div>
                    <div class="col-8">
                        <h5><a href="/projects/{{ $project->id }}">{{str_limit($project->title,20)}} </a></h5>
                        <small>{{ str_limit($project->description, 100)}}</small>
                    </div>
                    <div class="col-3">
                        <a href="/projects/{{ $project->id }}" class="btn btn-dark float-left mr-1"> <i class="far fa-eye"></i> View</a>

                        @auth
                        @if($user->can(['edit','delete'], $project))

                        <a href="/projects/{{$project->id}}/edit" class="btn btn-primary float-left mr-1"><i class="fas fa-edit"></i> Edit</a>

                        <form action="/projects/{{ $project->id }}" method="post">
                            @csrf
                            @method('DELETE')

                            <button type="submit" class="btn btn-danger float-left"><i class="far fa-trash-alt"></i> Delete</button>
                        </form>
                        @endif
                        @endauth


                    </div>
                </div>

            </div>

        </li>


        @empty
        <li>No projects yet</li>
        @endforelse
    </ul>

</div>




</div>

@endsection