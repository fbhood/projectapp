@extends('layouts.app')

@section('content')


<div class="container-fluid">

    <div class="row">

        @include('dashsidebar')


        <div class="col py-4">
            <div class="tab-content container" id="v-pills-tabContent">
                <div class="tab-pane fade show active" id="v-pills-dashboard" role="tabpanel" aria-labelledby="v-pills-dashboard-tab">
                    <div class="card">
                        <div class="card-header">Dashboard</div>

                        <div class="card-body">
                            @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                            @endif

                            You are logged in! Create a <a href="/projects/create">New Project </a>

                            <hr>

                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="v-pills-projects" role="tabpanel" aria-labelledby="v-pills-projects-tab">
                    <h1>Projects</h1>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th scope="col">#ID</th>
                                <th>Title</th>
                                <th>Description</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if($projects)

                            @forelse($projects as $project )
                            <tr>

                                <td scope="row">
                                    {{$project->id}}
                                </td>
                                <td>
                                    <a href="/projects/{{ $project->id }}">{{$project->title}} </a>
                                </td>
                                <td>
                                    {{$project->description}}
                                </td>
                                <td>

                                    <a href="/projects/{{ $project->id }}" class="btn btn-dark float-left mr-1"> <i class="far fa-eye"></i></a>

                                    @auth

                                    <a href="/projects/{{$project->id}}/edit" class="btn btn-primary float-left mr-1"><i class="fas fa-edit"></i></a>

                                    <form action="/projects/{{ $project->id }}" method="post">
                                        @csrf
                                        @method('DELETE')

                                        <button type="submit" class="btn btn-danger float-left"><i class="far fa-trash-alt"></i></button>
                                    </form>
                                    @endauth

                                </td>
                            </tr>

                            @empty
                            <li>No Projects yet</li>
                            @endforelse
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="tab-pane fade" id="v-pills-tasks" role="tabpanel" aria-labelledby="v-pills-tasks-tab">
                    <h1>Tasks</h1>
                    <table class="table table-striped">
                        <thead class="thead">
                            <tr>
                                <th scope="col">Project Name</th>
                                <th>Task Body</th>
                                <th>Status</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($project->tasks as $task )
                            <tr>
                                <td scope="row">
                                    {{$task->project->title}}

                                </td>
                                <td>
                                    <a href="/projects/{{ $project->id }}">{{$task->body}} </a>
                                </td>
                                <td>
                                    @if($task->completed === 0 )
                                    Incomplete
                                    @else
                                    Complete
                                    @endif
                                </td>
                                <td>

                                    <a href="/tasks/{{ $task->id }}" class="btn btn-dark float-left mr-1"> <i class="far fa-eye"></i></a>

                                    @auth

                                    <a href="/projects/{{$task->id}}/edit" class="btn btn-primary float-left mr-1"><i class="fas fa-edit"></i></a>

                                    <form action="/tasks/{{ $task->id }}" method="post">
                                        @csrf
                                        @method('DELETE')

                                        <button type="submit" class="btn btn-danger float-left"><i class="far fa-trash-alt"></i></button>
                                    </form>
                                    @endauth

                                </td>
                            </tr>

                            @endforeach

                        </tbody>
                    </table>


                </div>
                <div class="tab-pane fade" id="v-pills-collaborators" role="tabpanel" aria-labelledby="v-pills-collaborators-tab">
                    <h1>Collaborators</h1>
                    <p>List potential collaborators for your project</p>
                    <div class="row">

                        @foreach( $users as $collaborator )

                        <div class="col-4">
                            <div class="card">
                                <div class="card-body">
                                    <h3 class="card-title"> {{ $collaborator->name }}</h3>
                                    <p class="card-text">{{ $collaborator->email }}</p>
                                    <hr>
                                    <!-- Button trigger modal -->
                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#inviteCollaborator{{$collaborator->id}}">
                                        Invite {{$collaborator->name}}
                                    </button>

                                    <!-- Modal -->
                                    <div class="modal fade" id="inviteCollaborator{{$collaborator->id}}" tabindex="-1" role="dialog" aria-labelledby="inviteCollaborator{{$collaborator->id}}Label" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="inviteCollaborator{{$collaborator->id}}Label">Invite {{$collaborator->name}}</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">

                                                    <!--Invite user form-->

                                                    <form action="/projects/collaboration/invite" method="post">
                                                        @csrf
                                                        <div class="form-group">
                                                            <label for="from">From</label>
                                                            <input type="email" class="form-control" name="from" id="from" aria-describedby="emailFromHelper" placeholder="{{$user->email}}">
                                                            <small id="emailFromHelper" class="form-text text-muted"> Your email adress here </small>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="to">To</label>
                                                            <input type="email" class="form-control" name="to" id="to" aria-describedby="collaboratorEmailHelper{{$collaborator->id}}" placeholder="{{$collaborator->email}}" value="{{$collaborator->email}} ">
                                                            <small id="collaboratorEmailHelper{{$collaborator->id}}" class="form-text text-muted">The collaborator email address </small>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="message">Message</label>
                                                            <textarea class="form-control" name="message" id="message" rows="3" value="">Hi {{$collaborator->name}}, I need your help with this project {{$project->title}}</textarea>
                                                        </div>
                                                        <button type="submit" class="btn btn-primary">Invite</button>

                                                    </form>
                                                    <!--En d Invite user form-->

                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>




                        @endforeach
                    </div>

                </div>
                <div class="tab-pane fade" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">
                    <h1>Messages</h1>
                    <a href="/messages">Check your messages</a>



                </div>
                <div class="tab-pane fade" id="v-pills-reports" role="tabpanel" aria-labelledby="v-pills-reports-tab">
                    <h1>Reports</h1>
                </div>
                <div class="tab-pane fade" id="v-pills-settings" role="tabpanel" aria-labelledby="v-pills-settings-tab">
                    <h1>Settings</h1>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection