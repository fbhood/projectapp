@extends('layouts.app')

@section('content')

<div class="container py-5">

    <h1> {{$project->title}}
    </h1>
    <ul class="list-unstyled">
        <h4 class="pt-3">Tasks</h4>
        @forelse($tasks as $task)

        <li>{{ $task->body }} | <span class="badge bg-dark text-white">{{ $task->completed === 1 ? 'completed' : 'Pending'}}</span></li>

        @empty
        <li>no tasks</li>
        @endforelse
    </ul>
</div>
@endsection