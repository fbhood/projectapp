@extends('layouts.layout')

@section('title')

    Tasks | ProjectsApp

@endsection

@section('content')



<div class="jumbotron jumbotron-fluid">
    <div class="container">
        <h1 class="display-3">{{ $task->body}} </h1>
        <p class="lead">Project: {{ $task->project->title}}</p>
        
    </div>
</div>
@auth()
@if($task->project->user_id === auth()->id())


    <div class="container">

        <h4 class="card-title">You can see, ipsum dolor.</h4>
        <p class="card-text">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quis odit voluptatem neque blanditiis dolor omnis at qui eveniet, ex natus, veniam laboriosam incidunt! Voluptas suscipit, totam expedita odio id perspiciatis velit aliquid, temporibus provident a maxime exercitationem voluptatum assumenda laudantium, illo tempore recusandae! Ipsam quibusdam ut commodi provident dicta quas illo perferendis nostrum odio maxime! Cum perspiciatis maiores quisquam, quidem aperiam quaerat iste ullam a sequi culpa beatae impedit odio ipsa distinctio rem nihil labore natus, nesciunt magnam adipisci! Placeat, porro qui, eius nam facere magnam totam velit aliquam sequi ratione amet voluptate perspiciatis, reprehenderit culpa error! Quae, illum quidem.</p>
    </div>


@endif
@else
<div class="container">

<h4 class="card-title">Anyone see, ipsum dolor.</h4>
<p class="card-text">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quis odit voluptatem neque blanditiis dolor omnis at qui eveniet, ex natus, veniam laboriosam incidunt! Voluptas suscipit, totam expedita odio id perspiciatis velit aliquid, temporibus provident a maxime exercitationem voluptatum assumenda laudantium, illo tempore recusandae! Ipsam quibusdam ut commodi provident dicta quas illo perferendis nostrum odio maxime! Cum perspiciatis maiores quisquam, quidem aperiam quaerat iste ullam a sequi culpa beatae impedit odio ipsa distinctio rem nihil labore natus, nesciunt magnam adipisci! Placeat, porro qui, eius nam facere magnam totam velit aliquam sequi ratione amet voluptate perspiciatis, reprehenderit culpa error! Quae, illum quidem.</p>
</div>
@endauth





@endsection