@extends('layouts.layout')

@section('title')

    Contact us - ProjectsApp

@endsection



@section('content')
<div class="jumbotron jumbotron-fluid">
    <div class="container">
        <h1 class="display-3">Contact </h1>
        <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Harum, iure!</p>
        
    </div>
</div>
<div class="container">
<div class="row">
    <div class="col-6">
        <h4 class="card-title">Lorem, ipsum dolor.</h4>
        <p class="card-text">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quis odit voluptatem neque blanditiis dolor omnis at qui eveniet, ex natus, veniam laboriosam incidunt! Voluptas suscipit, totam expedita odio id perspiciatis velit aliquid, temporibus provident a maxime exercitationem voluptatum assumenda laudantium, illo tempore recusandae! Ipsam quibusdam ut commodi provident dicta quas illo perferendis nostrum odio maxime! Cum perspiciatis maiores quisquam, quidem aperiam quaerat iste ullam a sequi culpa beatae impedit odio ipsa distinctio rem nihil labore natus, nesciunt magnam adipisci! Placeat, porro qui, eius nam facere magnam totam velit aliquam sequi ratione amet voluptate perspiciatis, reprehenderit culpa error! Quae, illum quidem.</p>

    </div>
    <div class="col-6">
         <contact-form></contact-form>

    </div>
</div>
        

</div>




@endsection