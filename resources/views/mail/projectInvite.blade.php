@component('mail::message')
# Introduction

 invited you to collaborate o

@component('mail::button', ['url' => '/projects/'])
Accept Collaboration
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
