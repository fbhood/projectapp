@extends('layouts.layout')
@section('title')

{{ $page->title }}
@endsection



@section('content')
<div class="jumbotron jumbotron-fluid">
    <div class="container">
        <h1 class="display-3">{{ $page->title }} </h1>
        
    </div>
</div>
<div class="container">

        <p class="card-text">{{ $page->description }}</p>
</div>


@endsection
