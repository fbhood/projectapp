@extends('layouts.app')

@section('content')
<div class="container">
    

    <form action="/admin/pages" method="post">
        @csrf
        <div class="form-group">
        <label for="title-page">Title</label>
        <input type="text" class="form-control" name="title-page" id="title-page" aria-describedby="HelpPageTitle" placeholder="Page Title">
        <small id="HelpPageTitle" class="form-text text-muted">Add your page title</small>
        </div>

        <div class="form-group">
        <label for="page-description">Description</label>
        <textarea class="form-control" name="page-description" id="page-description" rows="5"></textarea>
        </div>
        <div class="form-group">
        <label for="page-image">Upload Image</label>
        <input type="file" class="form-control-file" name="page-image" id="page-image" placeholder="Image upload" aria-describedby="imageId">
        <small id="imageId" class="form-text text-muted">Help text</small>
        </div>

        <div class="form-group">
          <label for="status">Select Status</label>
          <select class="form-control" name="status" id="status">
            <option>Publish</option>
            <option>Draft</option>
            <option>Private</option>
          </select>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-info">Save</button>

        </div>

    </form>


</div>
@endsection