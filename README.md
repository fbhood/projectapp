# Projects APP
This is an open source projects management application build with Laravel and Mercurius.

## Installation

### Step 1.
> to run this project you need PHP7.2 installed as prerequisite.
begin by cloning the repository 

```bash
git clone https://bitbucket.org/fbhood/projectapp.git projectsApp
cd projectsApp && composer install
php artisan key:generate
```

### Step 2.
Next Create a database and reference it to the .env file. In the example below I have named the db projects_app

```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=projects_app
DB_USERNAME=root
DB_PASSWORD=

```

### Step 3.

Reference steps to add pusher keys for mercurius

### Step 4.


### Step 5.
