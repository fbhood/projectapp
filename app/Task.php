<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'completed', 'body',
    ];
    
    // 
    public function project(){

    return $this->belongsTo(Project::class);
    }

    public function user(){

        return $this->belongsTo(User::class);
    }

}
