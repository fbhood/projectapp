<?php

class WpApi
{
    protected $url = 'https://fabiopacifici.com/wp-json/wp/v2/work';


    public function importPosts($page = 1)
    {
        $posts = collect($this->getJson($this->url . 'posts/?_embed&filter[orderby]=modified&page=' . $page));
        foreach ($posts as $post) {
            $this->syncPost($post);
        }
    }

    protected function getJson($url)
    {
        $response = file_get_contents($url, false);
        return json_decode($response);
    }
}
$works = WpApi::importPosts();
var_dump($works);
