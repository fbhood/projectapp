<?php

namespace App\Providers;
use App\Project;
use App\Policies\ProjectPolicy;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use App\CreatePages;
use App\Policies\CreatePagesPolicy;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        //'App\Model' => 'App\Policies\ModelPolicy',
        Project::class => ProjectPolicy::class,
        CreatePages::class => CreatePagesPolicy::class,

    
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        Gate::define('create_pages.create', 'App\Policies\CreatePagesPolicy@create');
        Gate::define('create_pages.update', 'App\Policies\CreatePagesPolicy@update');
        Gate::define('create_pages.delete', 'App\Policies\CreatePagesPolicy@delete');

        //Define the gates for the given policies
        Gate::define('projects.update', 'App\Policies\ProjectPolicy@update');
        Gate::define('projects.delete', 'App\Policies\ProjectPolicy@delete');




    }
}
