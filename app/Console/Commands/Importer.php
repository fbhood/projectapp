<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class Importer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:wordpress';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import works from Wordpress API';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle($page)
    {
        //
        $page = ($this->argument('page')) ? $this->argument('page') : 1;
        $this->WpApiWork->importWorks($page);
    }
}
