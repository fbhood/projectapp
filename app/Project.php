<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'description', 'user_id'
    ];

    public function tasks()
    {

        return $this->hasMany(Task::class);
    }

    public function user()
    {

        return $this->belongsTo(User::class);
    }

    public function path()
    {
        return "/projects/{$this->id}";
    }
    /** 
     * Method to add a task to a project
     * @param string $body the body of the task
     * @param int $project_id The Id of the project
     * @param int $user_id The ID of the authenticated user
     * @return object return the new task  
     */
    public function addTask($body)
    {
        //dd($this->tasks()->create(['body' => $body, 'project_id' => $project_id, 'user_id' => $user_id]));
        //return $this->tasks()->create(['body' => $body]);

        $task = new Task();
        $task->user_id = auth()->id();
        $task->body = $body;
        $task->project_id = $this->id;
        return $task->save();

        //return Task::create(['body' => $body, 'project_id' => $project_id, 'user_id' => $user_id]);
    }
}
