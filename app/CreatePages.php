<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CreatePages extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'description','user_id','image', 'status'
    ];


    
}
