<?php

namespace App\Policies;

use App\User;
use App\CreatePages;
use Illuminate\Auth\Access\HandlesAuthorization;

class CreatePagesPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the create pages.
     *
     * @param  \App\User  $user
     * @param  \App\CreatePages  $createPages
     * @return mixed
     */
    public function view(User $user, CreatePages $createPages)
    {
   

    }

    /**
     * Determine whether the user can create create pages.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user, CreatePages $createPages)
    {
             // Restrict pages creation to Auth user. 
             return $user->id === $createPages->user_id;
    }

    /**
     * Determine whether the user can update the create pages.
     *
     * @param  \App\User  $user
     * @param  \App\CreatePages  $createPages
     * @return mixed
     */
    public function update(User $user, CreatePages $createPages)
    {
        if(! $user->id === 1 ) { return 'you cannot update this pages';}

        // Restrict pages update to user
        return $user->id === $createPages->user_id;

    }

    /**
     * Determine whether the user can delete the create pages.
     *
     * @param  \App\User  $user
     * @param  \App\CreatePages  $createPages
     * @return mixed
     */
    public function delete(User $user, CreatePages $createPages)
    {
        //
        if(! $user->id === 1 ) { return 'you cannot delete pages';}

        return $user->id === $createPages->user_id;

    }

    /**
     * Determine whether the user can restore the create pages.
     *
     * @param  \App\User  $user
     * @param  \App\CreatePages  $createPages
     * @return mixed
     */
    public function restore(User $user, CreatePages $createPages)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the create pages.
     *
     * @param  \App\User  $user
     * @param  \App\CreatePages  $createPages
     * @return mixed
     */
    public function forceDelete(User $user, CreatePages $createPages)
    {
        //
    }
}
