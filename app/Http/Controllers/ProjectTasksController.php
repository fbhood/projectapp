<?php

namespace App\Http\Controllers;

use App\Task;
use App\User;
use App\Project;
use App\Mail\ProjectTaskCompleted;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ProjectTasksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Project $project)
    {
        //
        $tasks = $project->tasks;
        if (auth()->user()->isNot($project->user)) {
            abort(403, "Restricted access, you can only view your projects");
        }
        return view('tasks.index', compact('tasks', 'project'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Show a view to create a new task

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Project $project)
    {
        //Store the task inside the db 

        request()->validate([
            'body' => 'required | min:5',

        ]);
        //dd(auth()->id());
        //dd($request->all());
        $project->addTask(request('body'));

        /*  $task = new Task();
        $task->user_id = auth()->id();
        $task->body = request('body');
        $task->project_id = $project->id;
        $task->save(); */
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project, Task $task, User $user)
    {

        return view('tasks.show', compact('project', 'task'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // Show the form to edit a task
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Task $task)
    {
        //Update the task

        $task->update([

            'completed' => request()->has('completed')

        ]);
        // dd($task);
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Task $task)
    {
        //Delete the task
        $task->delete();
        return back();
    }
}
