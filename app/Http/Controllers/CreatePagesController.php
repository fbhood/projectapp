<?php

namespace App\Http\Controllers;
use App\User;
use App\CreatePages;
use Illuminate\Http\Request;

class CreatePagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Return create page form

    return view('pages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, CreatePages $createPages, User $user)
    {
        //Validate the request
    
        Request()->validate([

            'title','description','status','image'
        ]);
        
        //store the page inside the db
        $createPages = new CreatePages; 
        $createPages->user_id  = auth()->id();
        $createPages->title = request('title');
        $createPages->description = request('description');
        $createPages->status = request('status');
        $createPages->image = request('image');

        $createPages->save();

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CreatePages  $createPages
     * @return \Illuminate\Http\Response
     */
    public function show(CreatePages $createPages)
    {
        $pages = CreatePages::all(); 
        if(! empty($pages)) :
        foreach( $pages as $page ):  
        return view('pages.show', compact( 'pages', 'page'));
        endforeach;
        endif;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CreatePages  $createPages
     * @return \Illuminate\Http\Response
     */
    public function edit(CreatePages $createPages)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CreatePages  $createPages
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CreatePages $createPages)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CreatePages  $createPages
     * @return \Illuminate\Http\Response
     */
    public function destroy(CreatePages $createPages)
    {
        //
    }
}
