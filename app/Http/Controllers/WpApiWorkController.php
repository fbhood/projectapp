<?php

namespace App\Http\Controllers;

use App\WpApiWork;
use Illuminate\Http\Request;

class WpApiWorkController extends Controller
{
  
    protected function syncWork($data)
    {
        $found = WpApiWork::where('wp_id', $data->id)->first();
    
        if (! $found) {
            return $this->createWork($data);
        }
    
        if ($found and $found->updated_at->format("Y-m-d H:i:s") < $this->carbonDate($data->modified)->format("Y-m-d H:i:s")) {
            return $this->updateWork($found, $data);
        }
    }
    
    protected function carbonDate($date)
    {
        return Carbon::parse($date);
    }
    
    protected function createWork($data)
    {
        $work = new WpApiWork();
        $work->id = $data->id;
        $work->wp_id = $data->id;
        //$work->user_id = $this->getAuthor($data->_embedded->author);
        $work->title = $data->title->rendered;
        //$work->slug = $data->slug;
        //$work->featured_image = $this->featuredImage($data->_embedded);
        //$work->featured = ($data->sticky) ? 1 : null;
        //$work->excerpt = $data->excerpt->rendered;
        $work->content = $data->content->rendered;
        //$work->format = $data->format;
        $work->status = 'publish';
        //$work->publishes_at = $this->carbonDate($data->date);
        $work->created_at = $this->carbonDate($data->date);
        $work->updated_at = $this->carbonDate($data->modified);
        //$work->category_id = $this->getCategory($data->_embedded->{"wp:term"});
        $work->save();
        //$this->syncTags($work, $data->_embedded->{"wp:term"});
        return $work;
    }
    
    public function featuredImage($data)
    {
    if (property_exists($data, "wp:featuredmedia")) {
        $data = head($data->{"wp:featuredmedia"});
        if (isset($data->source_url)) {
            return $data->source_url;
        }
    }
    return null;
    }
    
    public function getCategory($data)
    {
    $category = collect($data)->collapse()->where('taxonomy', 'category')->first();
    $found = Category::where('wp_id', $category->id)->first();
    if ($found) {
        return $found->id;
    }
    $cat = new Category();
    $cat->id = $category->id;
    $cat->wp_id = $category->id;
    $cat->name = $category->name;
    $cat->slug = $category->slug;
    $cat->description = '';
    $cat->save();
    return $cat->id;
    }
    
    private function syncTags(WpApiWork $work, $tags)
    {
    $tags = collect($tags)->collapse()->where('taxonomy', 'post_tag')->pluck('name')->toArray();
    if (count($tags) > 0) {
        $work->setTags($tags);
    }
    }
}
