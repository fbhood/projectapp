<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\CreatePages;
use App\Project;
use App\Task;
use App\User;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(CreatePages $createPages, User $user, Project $project, Task $task)
    {
        $pages = CreatePages::where(
            'user_id',
            auth()->id()
        )->get();
        $users = User::all();
        $projects = Project::all();
        $tasks = Task::all();
        if (auth()->id() !== 1) {
            abort(403, "Admin only Access.");
        }
        return view('admin.adminpanel', compact('pages', 'users', 'projects', 'tasks'));
    }
}
