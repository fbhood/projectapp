<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;
use App\User;
use App\Task;
use Illuminate\Support\Facades\Mail;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Project $project, User $user, Task $task)
    {

        $projects = Project::where(
            'user_id',
            auth()->id()
        )->get();

        $tasks = Task::where(
            'user_id',
            auth()->id()
        )->get();

        $users = User::all();

        $user = auth()->user();


        return view('dashboard', compact('projects', 'project', 'users', 'user', 'tasks'));
    }

    public function mercurius()
    {

        return view('vendor.mercurius.mercurius');
    }
}
