<?php

namespace App\Http\Controllers;

use App\User;
use App\Project;
use App\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Mail\ProjectInvite;
use Illuminate\Support\Facades\Auth;

class ProjectsController extends Controller
{
    /*TODO:  Remove - use a route group middleware instead
        public function __construct()
    {

        $this->middleware('auth')->except('index', 'show');
    } */


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Project $project, User $user, Task $task)
    {
        // Return view to Create a new Project 


        $projects = Auth::user()->project;
        //dd($projects);

        $tasks = Task::all();

        return view('projects.indexList', compact('projects', 'tasks', 'user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('projects.create-project');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // request()->all(); # get all info stored
        $attributes = request()->validate([
            'title' => 'required | min:5',
            'description' => 'required | min:20'
        ]);


        auth()->user()->project()->create($attributes);

        /*  $attributes['user_id'] = auth()->id();
       
        Project::create($attributes); */




        return redirect('/projects');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project, Task $task)
    {
        //Return the single project view

        if (auth()->user()->isNot($project->user)) {
            abort(403, "Restricted access, you can only view your projects");
        }
        return view('projects.show', compact('project', 'task'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $project, User $user)
    {
        if ($project->user_id === auth()->id()) {

            return view('projects.edit', compact('project'));
        }
        abort(403);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Project $project, User $user)
    {
        //Update the project on save
        //

        $project->update(request(['title', 'description']));

        //Code below was comment out as equeal to the simplyfied version above
        //$project = Project::find($project->id);
        //$project->title = request('title');
        //$project->description = request('description');
        //$project->save();
        return redirect('/projects');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Project $project
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project, User $user)
    {

        $project->delete();
        return back();
    }


    public function invite(Project $project, User $user)

    {
        $invite = new ProjectInvite($project, $user);
        \Mail::to('fabio@fab.fab')->send($invite);
    }
}
