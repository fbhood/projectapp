<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Auth\Authenticatable;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Auth;

class ProjectsTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    /** @test */
    public function only_authenticate_users_can_create_a_project()
    {
        $attributes = factory('App\Project')->raw();
        $this->post('/projects', $attributes)->assertRedirect('/login');
    }

    /** @test  */
    public function gests_cannot_view_projects()
    {

        $this->get('/projects')->assertRedirect('/login');
    }
    /** @test  */
    public function gests_cannot_view_a_single_project()
    {
        $project = factory('App\Project')->create();
        $this->get($project->path())->assertRedirect('/login');
    }
    /** @test  */

    public function authenticated_users_can_manage_projects()
    {
        # code...
        $this->withoutExceptionHandling();


        $user = factory(User::class)->create();

        //$this->actingAs($user);
        $this->signUser($user);
        $data = [
            'user_id' => $user['id'],
            'title' => $this->faker->sentence,
            'description' => $this->faker->paragraph
        ];

        $this->post('/projects', $data)->assertRedirect('/projects');

        $this->assertDatabaseHas('projects', $data);

        $this->get('/projects')->assertSee(str_limit($data['title'], 20));
    }



    /** @test */
    public function a_project_requires_a_title()
    {

        //$this->actingAs(factory('App\User')->create());
        $this->signUser();
        $attributes = factory('App\Project')->raw(['title' => ' ']);
        $this->post('/projects', $attributes)->assertSessionHasErrors('title');
    }
    /** @test */
    public function a_project_requires_a_description()
    {

        //$this->actingAs(factory('App\User')->create());
        $this->signUser();
        $attributes = factory('App\Project')->raw(['description' => ' ']);
        $this->post('/projects', $attributes)->assertSessionHasErrors('description');
    }
    /** @test */
    public function a_user_can_view_their_project()
    {

        $this->withExceptionHandling();
        $this->be(factory('App\User')->create());
        $project = factory('App\Project')->create(['user_id' => auth()->id()]);

        $this->get($project->path())
            ->assertSee($project->title)
            ->assertSee($project->description);
    }
    /** @test */
    public function an_authenticated_user_cannot_view_projects_of_others()
    {
        $this->be(factory('App\User')->create());
        $this->withExceptionHandling();

        $project = factory('App\Project')->create();

        $this->get($project->path())->assertStatus(403);
    }
}
