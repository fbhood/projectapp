<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProjectsTasksTest extends TestCase
{
    use RefreshDatabase;
    /** @test */
    public function a_project_can_have_tasks()
    {
        $this->signUser();
        $this->withoutExceptionHandling();
        $project = factory('App\Project')->create(['user_id' => auth()->id()]);

        $this->post($project->path() . '/tasks', ['body' => 'lorem task']);

        $this->get($project->path() . '/tasks')->assertSee('lorem task');
    }
}
