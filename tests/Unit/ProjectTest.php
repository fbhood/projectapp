<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProjectTest extends TestCase
{
    use RefreshDatabase;
    /** @test */
    public function a_project_has_a_path()
    {
        $project = factory('App\Project')->create();

        $this->assertEquals('/projects/' . $project->id, $project->path());
    }
    /** @test */
    public function a_project_belongs_to_a_user()
    {
        $project = factory('App\Project')->create();

        $this->assertInstanceOf('App\User', $project->user);
    }
    /** @test */

    public function can_add_a_task()
    {
        $this->withExceptionHandling();
        $this->signUser();
        $project = factory('App\Project')->create(['user_id' => auth()->id()]);

        $project->addTask('lorem task');

        $this->assertCount(1, $project->tasks);
    }
}
